﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace BinarySearchTreeWorkshop
{
    public class BinarySearchTree : IBinarySearchTree
    {
        public BinaryTreeNode Root { get; private set; }
        private int count;
        public void Insert(int value)
        {
            if (this.Root == null)
            {
                this.Root = new BinaryTreeNode(value);
            }
            else
            {
                FindWhereToAdd(this.Root, value);
            }
            count++;
        }
        public void FindWhereToAdd(BinaryTreeNode current, int value)
        {

            if (current.Compare(current, value))
            {

                if (current.RightChild == null)
                {
                    current.RightChild = new BinaryTreeNode(value);
                }
                else
                {
                    FindWhereToAdd(current.RightChild, value);
                }
            }
            else
            {

                if (current.LeftChild == null)
                {
                    current.LeftChild = new BinaryTreeNode(value);
                }
                else
                {
                    FindWhereToAdd(current.LeftChild, value);
                }
            }
        }

        public BinaryTreeNode Search(int value)
        {
            if (this.Root == null)
            {
                return null;
            }
            if (Root.Value == value)
            {
                return Root;
            }
            else if (Root.Compare(Root, value))
            {
                Root = Root.RightChild;

                if (Root != null)
                {
                    if (Root.Value == value)
                    {
                        return Root;
                    }
                    return Search(value);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                Root = Root.LeftChild;

                if (Root != null)
                {
                    if (Root.Value == value)
                    {
                        return Root;
                    }
                    else
                    {
                        return Search(value);
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public List<int> InOrder()
        {
            throw new NotImplementedException();
        }

        public List<int> PostOrder()
        {
            throw new NotImplementedException();
        }

        public List<int> PreOrder()
        {
            throw new NotImplementedException();

        }

        public List<int> Bfs()
        {
            throw new NotImplementedException();
        }

        public int Height()
        {
            if (this.Root == null)
            {
                return -1;
            }
            else if (this.Root.LeftChild == null && this.Root.RightChild == null)
            {
                return 0;
            }

            return 1;
        }

        public BinaryTreeNode Remove(int value)
        {
            throw new NotImplementedException();
        }

        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }


    }
}
