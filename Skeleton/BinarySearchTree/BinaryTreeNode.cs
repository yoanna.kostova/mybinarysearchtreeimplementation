﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinarySearchTreeWorkshop
{
    public class BinaryTreeNode
    {
        public BinaryTreeNode(int value)
        {
            this.Value = value;
        }

        public int Value { get; private set; }
        public BinaryTreeNode LeftChild { get; set; }
        public BinaryTreeNode RightChild { get; set; }

        public bool Compare(BinaryTreeNode node, int valueToBeComparedWith)
        {
            //If the value we are comparing equals the upper value in the tree, the value is considered to be higher, so it goes right.
            if (node.Value < valueToBeComparedWith || node.Value == valueToBeComparedWith)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
    }
}
