﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BinarySearchTreeWorkshop
{
    public interface IBinarySearchTree : IEnumerable
    {
        BinaryTreeNode Root { get; }

        void Insert(int value);

        BinaryTreeNode Search(int value);

        public List<int> InOrder();

        public List<int> PostOrder();

        public List<int> PreOrder();

        public List<int> Bfs();

        public int Height();

        public BinaryTreeNode Remove(int value);
    }
}