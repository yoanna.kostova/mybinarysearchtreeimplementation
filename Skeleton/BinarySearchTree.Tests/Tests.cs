using BinarySearchTreeWorkshop;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BinarySearchTreeTests
{
    [TestClass]
    public class Tests
    {
        BinarySearchTree testTree = new BinarySearchTree();

        [TestMethod]
        public void InsertShould_UpdateRoot_WhenTreeEmpty()
        {
            //Act
            testTree.Insert(5);

            //Assert
            Assert.AreEqual(5, testTree.Root.Value);
        }

        [TestMethod]
        public void InsertShould_AddLeftNodeCorrectly_WhenTreeHasRootOnly()
        {
            //Arrange
            testTree.Insert(5);

            //Act
            testTree.Insert(4);

            //Assert
            Assert.AreEqual(4, testTree.Root.LeftChild.Value);
        }

        [TestMethod]
        public void InsertShould_AddRightNodeCorrectly_WhenTreeHasRootOnly()
        {
            //Arrange
            testTree.Insert(5);

            //Act
            testTree.Insert(7);

            //Assert
            Assert.AreEqual(7, testTree.Root.RightChild.Value);
        }

        [TestMethod]
        public void InsertShould_AddLeftNodeCorrectly_WhenTreeHasMultipleNodes()
        {
            //Arrange
            PrepareTestTree();

            //Act
            testTree.Insert(25);

            //Assert
            Assert.AreEqual(25, testTree.Root.LeftChild.LeftChild.RightChild.Value);
        }

        [TestMethod]
        public void InsertShould_AddRightNodeCorrectly_WhenTreeHasMultipleNodes()
        {
            //Arrange
            PrepareTestTree();

            //Act
            testTree.Insert(65);

            //Assert
            Assert.AreEqual(65, testTree.Root.RightChild.LeftChild.RightChild.Value);
        }

        [TestMethod]
        public void SearchShould_ReturnNull_WhenTreeEmpty()
        {
            //Act
            var node = testTree.Search(5);

            //Assert
            Assert.IsNull(node);
        }

        [TestMethod]
        public void SearchShould_ReturnNull_WhenNodeDoesNotExist()
        {
            //Arrange
            PrepareTestTree();

            //Act
            var node = testTree.Search(5);

            //Assert
            Assert.IsNull(node);
        }

        [TestMethod]
        public void SearchShould_ReturnNode_WhenTreeHasRootOnly()
        {
            //Arrange
            testTree.Insert(10);

            //Act
            var node = testTree.Search(10);

            //Assert
            Assert.AreEqual(10, node.Value);
        }

        [TestMethod]
        public void SearchShould_ReturnNode_WhenTreeHasMultipleNodes()
        {
            //Arrange
            PrepareTestTree();

            //Act
            var node = testTree.Search(60);

            //Assert
            Assert.AreEqual(60, node.Value);
        }

        [TestMethod]
        public void PreOrderShould_ReturnElementsInRightSequence()
        {
            //Arrange
            PrepareTestTree();
            var expected = new int[] { 50, 30, 20, 40, 70, 60, 80, 72, 71 };

            //Act
            var result = testTree.PreOrder();

            //Assert
            CollectionAssert.AreEqual(expected, result.ToArray());
        }

        [TestMethod]
        public void PreOrderShould_ReturnEmptyList_WhenTreeIsEmpty()
        {
            //Arrange
            var expected = new int[0];

            //Act
            var result = testTree.PreOrder();

            //Assert
            CollectionAssert.AreEqual(expected, result.ToArray());
        }

        [TestMethod]
        public void InOrderShould_ReturnElementsInRightSequence()
        {
            //Arrange
            PrepareTestTree();
            var expected = new int[] {20, 30, 40, 50, 60, 70, 71, 72, 80};

            //Act
            var result = testTree.InOrder();

            //Assert
            CollectionAssert.AreEqual(expected, result.ToArray());
        }

        [TestMethod]
        public void InOrderShould_ReturnEmptyList_WhenTreeIsEmpty()
        {
            //Arrange
            var expected = new int[0];

            //Act
            var result = testTree.InOrder();

            //Assert
            CollectionAssert.AreEqual(expected, result.ToArray());
        }

        [TestMethod]
        public void PostORderShould_ReturnElementsInRightSequence()
        {
            //Arrange
            PrepareTestTree();
            var expected = new int[] { 20, 40, 30, 60, 71, 72, 80, 70, 50 };

            //Act
            var result = testTree.PostOrder();

            //Assert
            CollectionAssert.AreEqual(expected, result.ToArray());
        }

        [TestMethod]
        public void PostORderShould_ReturnEmptyList_WhenTreeIsEmpty()
        {
            //Arrange
            var expected = new int[0] { };

            //Act
            var result = testTree.PostOrder();

            //Assert
            CollectionAssert.AreEqual(expected, result.ToArray());
        }

        [TestMethod]
        public void BfsShould_ReturnElementsInRightSequence()
        {
            //Arrange
            PrepareTestTree();
            var expected = new int[] { 50, 30, 70, 20, 40, 60, 80, 72, 71 };

            //Act
            var result = testTree.Bfs();

            //Assert
            CollectionAssert.AreEqual(expected, result.ToArray());
        }

        [TestMethod]
        public void BfsShould_ReturnEmptyList_WhenTreeIsEmpty()
        {
            //Arrange
            var expected = new int[0] { };

            //Act
            var result = testTree.Bfs();

            //Assert
            CollectionAssert.AreEqual(expected, result.ToArray());
        }

        [TestMethod]
        public void HeightShould_ReturnMinusOne_WhenTreeIsEmpty()
        {
            //Act & Assert
            Assert.AreEqual(-1, testTree.Height());
        }

        [TestMethod]
        public void HeightShould_ReturnZero_WhenTreeHasRootOnly()
        {
            //Arrange
            testTree.Insert(5);

            //Act & Assert
            Assert.AreEqual(0, testTree.Height());
        }

        [TestMethod]
        public void HeightShould_ReturnCorrectHeight()
        {
            //Arrange
            PrepareTestTree();

            //Act & Assert
            Assert.AreEqual(4, testTree.Height());
        }

        [TestMethod]
        public void RemoveShould_ReturnNull_WhenTreeIsEmpty()
        {
            //Act
            var removed = testTree.Remove(5);

            //Assert
            Assert.IsNull(removed);
        }

        [TestMethod]
        public void RemoveShould_ReplaceRoot_WhenValueToBeRemovedIsInRoot()
        {
            //Arrange
            PrepareTestTree();

            //Act
            var removed = testTree.Remove(50);

            //Assert
            Assert.AreEqual(60, removed.Value);
        }

        [TestMethod]
        public void RemoveShould_MaintainCorrectOrdering_WhenValuePresentAndHasRightChildOnly()
        {
            //Arrange
            PrepareTestTree();
            testTree.Insert(42);

            //Act
            var removed = testTree.Remove(40);

            //Assert
            Assert.AreEqual(42, testTree.Root.LeftChild.RightChild.Value);
        }

        [TestMethod]
        public void RemoveShould_MaintainCorrectOrdering_WhenValuePresentAndHasLeftChildOnly()
        {
            //Arrange
            PrepareTestTree();
            testTree.Insert(39);

            //Act
            var removed = testTree.Remove(40);

            //Assert
            Assert.AreEqual(39, testTree.Root.LeftChild.RightChild.Value);
        }

        [TestMethod]
        public void RemoveShould_MaintainCorrectOrdering_WhenValuePresentAndHasBothChildren()
        {
            //Arrange
            PrepareTestTree();

            //Act
            var removed = testTree.Remove(70);

            //Assert
            Assert.AreEqual(71, testTree.Root.RightChild.Value);
            Assert.IsNull(testTree.Root.RightChild.RightChild.LeftChild.LeftChild);
        }

        private void PrepareTestTree()
        {
            testTree.Insert(50);
            testTree.Insert(30);
            testTree.Insert(20);
            testTree.Insert(40);
            testTree.Insert(70);
            testTree.Insert(60);
            testTree.Insert(80);
            testTree.Insert(72);
            testTree.Insert(71);
        }
    }
}
